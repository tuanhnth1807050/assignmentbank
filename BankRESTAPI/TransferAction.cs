﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankRESTAPI
{
    public class NewTransfer
    {
        public int clientCode { get; set; }
        public string clientPassword { get; set; }
        public int clientPin { get; set; }
        public int partnerCode { get; set; }
        public string partnerPassword { get; set; }
        public int transferCode { get; set; }
        public string transferName { get; set; }
        public bool transferType { get; set; }
        public decimal transferAmount { get; set; }
    }

    public class ClientGetTransferHistory
    {
        public int clientCode { get; set; }
        public string clientPassword { get; set; }
    }

    public class ClientGetTransferHistoryResult
    {
        public int partnerCode { get; set; }
        public int transferCode { get; set; }
        public string transferName { get; set; }
        public bool transferType { get; set; }
        public decimal transferAmount { get; set; }
    }

    public class PartnerGetTransferHistory
    {
        public int partnerCode { get; set; }
        public string partnerPassword { get; set; }
    }

    public class PartnerGetTransferHistoryResult
    {
        public int clientCode { get; set; }
        public int transferCode { get; set; }
        public string transferName { get; set; }
        public bool transferType { get; set; }
        public decimal transferAmount { get; set; }
    }

    public class TransferAction
    {
        BankDataDataContext bank = new BankDataDataContext();

        public bool AddNewTransfer(NewTransfer newTransfer)
        {
            try
            {
                Client transferClient =
                    (from client in bank.Clients
                     where client.clientCode == newTransfer.clientCode
                     && client.clientPassword == newTransfer.clientPassword
                     && client.pin == newTransfer.clientPin
                     select client).DefaultIfEmpty().Single();

                Partner transferPartner =
                    (from partner in bank.Partners
                     where partner.partnerCode == newTransfer.partnerCode
                     && partner.partnerPassword == newTransfer.partnerPassword
                     select partner).DefaultIfEmpty().Single();

                if(transferClient != null && transferPartner != null)
                {
                    if (newTransfer.transferType)
                    {
                        transferClient.deposits -= TotalPriceCaculate(newTransfer.transferAmount);
                        if (transferClient.deposits < 0)
                        {
                            return false;
                        }
                        transferPartner.deposits += newTransfer.transferAmount;
                    }
                    else
                    {
                        transferPartner.deposits -= TotalPriceCaculate(newTransfer.transferAmount);
                        if (transferPartner.deposits < 0)
                        {
                            return false;
                        }
                        transferClient.deposits += newTransfer.transferAmount;
                    }

                    bank.TransferHistories.InsertOnSubmit(
                        CreateNewTransferHistory(
                            transferClient.clientId,
                            transferPartner.partnerId,
                            newTransfer.transferName,
                            newTransfer.transferType,
                            newTransfer.transferAmount,
                            newTransfer.transferCode)
                        );
                    bank.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public decimal TotalPriceCaculate(decimal transferAmount)
        {
            double transAmount = (double)transferAmount;

            if (transferAmount <= 100000)
            {
                return (decimal)(transAmount + 10000);
            }
            else if (transferAmount <= 500000)
            {
                return (decimal)(transAmount + transAmount * 0.02);
            }
            else if (transferAmount <= 1000000)
            {
                return (decimal)(transAmount + transAmount * 0.015);
            }
            else if (transferAmount <= 5000000)
            {
                return (decimal)(transAmount + transAmount * 0.01);
            }
            else 
            {
                return (decimal)(transAmount + transAmount * 0.005);
            }
        }

        public TransferHistory CreateNewTransferHistory (
            int clientId,
            int partnerId,
            string transferName,
            bool transferType,
            decimal transferAmount,
            int billCode)
        {
            TransferHistory transHistory = new TransferHistory();
            transHistory.clientId = clientId;
            transHistory.partnerId = partnerId;
            transHistory.transferName = transferName;
            transHistory.transferType = transferType;
            transHistory.transferAmount = transferAmount;
            transHistory.billCode = billCode;
            return transHistory;
        }

        public List<ClientGetTransferHistoryResult> ClientGetTransferHistory(ClientGetTransferHistory data)
        {
            try
            {
                Client transferClient =
                    (from client in bank.Clients
                     where client.clientCode == data.clientCode
                     && client.clientPassword == data.clientPassword
                     select client).DefaultIfEmpty().Single();

                if (transferClient != null)
                {
                    List<ClientGetTransferHistoryResult> results = new List<ClientGetTransferHistoryResult>();
                    List<TransferHistory> transHis =
                        (from trans in bank.TransferHistories
                         where trans.clientId == transferClient.clientId
                         select trans).ToList();

                    foreach(TransferHistory trans in transHis)
                    {
                        ClientGetTransferHistoryResult result = new ClientGetTransferHistoryResult();
                        result.partnerCode = trans.partnerId;
                        result.transferCode = trans.billCode;
                        result.transferName = trans.transferName;
                        result.transferType = trans.transferType;
                        result.transferAmount = trans.transferAmount;
                        results.Add(result);
                    }
                    return results;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public List<PartnerGetTransferHistoryResult> PartnerGetTransferHistory(PartnerGetTransferHistory data)
        {
            try
            {
                Partner transferPartner =
                    (from partner in bank.Partners
                     where partner.partnerCode == data.partnerCode
                     && partner.partnerPassword == data.partnerPassword
                     select partner).DefaultIfEmpty().Single();

                if (transferPartner != null)
                {
                    List<PartnerGetTransferHistoryResult> results = new List<PartnerGetTransferHistoryResult>();
                    List<TransferHistory> transHis =
                        (from trans in bank.TransferHistories
                         where trans.clientId == transferPartner.partnerId
                         select trans).ToList();

                    foreach (TransferHistory trans in transHis)
                    {
                        PartnerGetTransferHistoryResult result = new PartnerGetTransferHistoryResult();
                        result.clientCode = trans.clientId;
                        result.transferCode = trans.billCode;
                        result.transferName = trans.transferName;
                        result.transferType = trans.transferType;
                        result.transferAmount = trans.transferAmount;
                        results.Add(result);
                    }
                    return results;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}